class DefaultEventhandlers;
class CfgPatches
{
	class EOD_SUIT
	{
		units[]=
		{
			"EOD_engineer",
			"EOD9_HELMET_Lights_VEH"
		};
		author="Mr Ewok, NorX Aengell,Burnes15th";
		weapons[]=
		{
			"EOD9_HELMET",
			"EOD_SUIT_vest",
			"EOD_Uniform",
			"EOD9_HELMET_Base"
		};
		requiredVersion=0.1;
		requiredAddons[]={};
	};
};
class Extended_PostInit_EventHandlers
{
	EOD_SUIT_Init="if (hasInterface) then {[] execVM '\EOD_SUIT\XENO_Init_Script_EOD_SUIT.sqf'}";
};
class EODS_SoundRegulation_settings
{
};
class CfgSounds
{
	class Redbreather2_resp_Low
	{
		name="Redbreather2_resp_Low";
		sound[]=
		{
			"EOD_SUIT\Sound\Redbreather2_resp_Low_1shot.wss",
			4,
			1,
			1
		};
		titles[]={};
	};
	class Redbreather3_resp_High
	{
		name="Redbreather3_resp_High";
		sound[]=
		{
			"EOD_SUIT\Sound\Redbreather3_resp_High_1shot.wss",
			4,
			1,
			1
		};
		titles[]={};
	};
};
class CfgVehicleClasses
{
	class EWK_veh_class
	{
		displayName="$STR_EODS_EOD_units";
	};
};
class UniformSlotInfo;
class CfgVehicles
{
	class Man;
	class CAManBase: Man
	{
		class HitPoints
		{
			class HitHead;
			class HitBody;
			class HitHands;
			class HitLegs;
		};
	};
	class SoldierWB: CAManBase
	{
		threat[]={1,0.1,0.1};
	};
	class B_Soldier_base_F: SoldierWB
	{
	};
	class B_engineer_F: B_Soldier_base_F
	{
	};
	class EOD_engineer: B_engineer_F
	{
		scope=2;
		author="MrEwok";
		displayName="$STR_EOD_engineer";
		vehicleClass="EWK_veh_class";
		engineer=1;
		attendant=0;
		uniformAccessories[]={};
		nakedUniform="U_BasicBody";
		uniformClass="EOD_Uniform";
		model="\EOD_SUIT\Models\EOD_Uniform.p3d";
		hiddenSelections[]=
		{
			"insignia"
		};
		hiddenSelectionsTextures[]={};
		weapons[]=
		{
			"Throw",
			"Put"
		};
		respawnWeapons[]=
		{
			"Throw",
			"Put"
		};
		Items[]=
		{
			"FirstAidKit"
		};
		RespawnItems[]=
		{
			"FirstAidKit"
		};
		magazines[]={};
		respawnMagazines[]={};
		linkedItems[]=
		{
			"EOD9_HELMET",
			"EOD_SUIT_vest",
			"ItemMap",
			"ItemCompass",
			"ItemWatch",
			"ItemRadio"
		};
		respawnLinkedItems[]=
		{
			"EOD9_HELMET",
			"EOD_SUIT_vest",
			"ItemMap",
			"ItemCompass",
			"ItemWatch",
			"ItemRadio"
		};
		backpack="";
		class Wounds
		{
			tex[]={};
			mat[]=
			{
				"eod_suit\textureswip\green_tex2.rvmat",
				"eod_suit\textureswip\Green_Tex_wound.rvmat",
				"eod_suit\textureswip\Green_Tex_wound.rvmat",
				"A3\Characters_F\Heads\Data\hl_white_bald_muscular.rvmat",
				"A3\Characters_F\Heads\Data\hl_white_bald_muscular_injury.rvmat",
				"A3\Characters_F\Heads\Data\hl_white_bald_muscular_injury.rvmat",
				"A3\Characters_F\Heads\Data\hl_black_bald_muscular.rvmat",
				"A3\Characters_F\Heads\Data\hl_black_bald_muscular_injury.rvmat",
				"A3\Characters_F\Heads\Data\hl_black_bald_muscular_injury.rvmat",
				"A3\Characters_F\Heads\Data\hl_white_hairy_muscular.rvmat",
				"A3\Characters_F\Heads\Data\hl_white_hairy_muscular_injury.rvmat",
				"A3\Characters_F\Heads\Data\hl_white_hairy_muscular_injury.rvmat",
				"A3\Characters_F\Heads\Data\hl_white_old.rvmat",
				"A3\Characters_F\Heads\Data\hl_white_old_injury.rvmat",
				"A3\Characters_F\Heads\Data\hl_white_old_injury.rvmat",
				"A3\Characters_F\Heads\Data\hl_asian_bald_muscular.rvmat",
				"A3\Characters_F\Heads\Data\hl_asian_bald_muscular_injury.rvmat",
				"A3\Characters_F\Heads\Data\hl_asian_bald_muscular_injury.rvmat"
			};
		};
	};
	class LandVehicle;
	class Car: LandVehicle
	{
		class NewTurret;
	};
	class Car_F: Car
	{
		class AnimationSources;
		class Turrets
		{
			class MainTurret: NewTurret
			{
			};
		};
		class HitPoints
		{
			class HitLFWheel;
			class HitLF2Wheel;
			class HitRFWheel;
			class HitRF2Wheel;
			class HitGlass1;
			class HitGlass2;
			class HitGlass3;
			class HitGlass4;
			class HitGlass5;
			class HitGlass6;
		};
	};
	class EOD9_HELMET_Lights_VEH_base: Car_F
	{
		class SpeechVariants
		{
			class Default
			{
				speechSingular[]=
				{
					"veh_vehicle_armedcar_s"
				};
				speechPlural[]=
				{
					"veh_vehicle_armedcar_p"
				};
			};
		};
		textSingular="$STR_A3_nameSound_veh_vehicle_armedcar_s";
		textPlural="$STR_A3_nameSound_veh_vehicle_armedcar_p";
		nameSound="veh_vehicle_armedcar_s";
		author="MrEwok";
		_generalMacro="EOD9_HELMET_Lights_VEH_base";
		model="\EOD_SUIT\Models\EOD9_HELMET_Lights_VEH.p3d";
		Picture="";
		Icon="";
		mapSize=6;
		displayName="EOD9_HELMET_Lights_VEH_base";
		scope=1;
		faction="BLU_F";
		side=1;
		vehicleClass="Car";
		crew="B_Soldier_F";
		typicalCargo[]=
		{
			"B_Soldier_F"
		};
		WaterLeakiness=10;
		maxFordingDepth=0.5;
		waterResistance=1;
		wheelCircumference=2.8340001;
		antiRollbarForceCoef=12;
		antiRollbarForceLimit=10;
		antiRollbarSpeedMin=20;
		antiRollbarSpeedMax=50;
		crewVulnerable=1;
		crewCrashProtection=0.15000001;
		weapons[]={};
		magazines[]={};
		damperSize=0.2;
		damperForce=1;
		damperDamping=1;
		armor=80;
		damageResistance=0.00562;
		turnCoef=2.5;
		steerAheadPlan=0.2;
		steerAheadSimul=0.40000001;
		predictTurnPlan=0.89999998;
		predictTurnSimul=0.5;
		brakeDistance=1;
		terrainCoef=1.5;
		wheelDamageThreshold=0.69999999;
		wheelDestroyThreshold=0.99000001;
		wheelDamageRadiusCoef=0.94999999;
		wheelDestroyRadiusCoef=0.44999999;
		cost=200000;
		precision=15;
		armorGlass=0.5;
		armorWheels=0.1;
		soundServo[]=
		{
			"A3\sounds_f\dummysound",
			0.0099999998,
			1,
			10
		};
		soundEnviron[]=
		{
			"",
			0.56234133,
			1
		};
		transportMaxBackpacks=0;
		transportSoldier=0;
		class Library
		{
			libTextDesc="EOD9_HELMET_Lights_VEH_base";
		};
		castDriverShadow="false";
		driverAction="passenger_injured_medevac_truck01";
		cargoAction[]={};
		threat[]={0.80000001,0.60000002,0.30000001};
		driverLeftHandAnimName="drivewheel";
		driverRightHandAnimName="";
		driverLeftLegAnimName="";
		driverRightLegAnimName="pedal_thrust";
		class TransportMagazines
		{
		};
		class TransportItems
		{
		};
		class TransportWeapons
		{
		};
		idleRpm=800;
		redRpm=4500;
		brakeIdleSpeed=1.78;
		fuelCapacity=250;
		class complexGearbox
		{
			GearboxRatios[]=
			{
				"R1",
				-5.75,
				"N",
				0,
				"D1",
				4.3000002,
				"D2",
				2.3,
				"D3",
				1.5,
				"D4",
				1,
				"D5",
				0.73000002
			};
			TransmissionRatios[]=
			{
				"High",
				6.7589998
			};
			gearBoxMode="auto";
			moveOffGear=1;
			driveString="D";
			neutralString="N";
			reverseString="R";
		};
		simulation="carx";
		dampersBumpCoef=6;
		differentialType="all_limited";
		frontRearSplit=0.5;
		frontBias=1.3;
		rearBias=1.3;
		centreBias=1.3;
		clutchStrength=20;
		enginePower=276;
		maxOmega=471;
		peakTorque=1253;
		dampingRateFullThrottle=0.079999998;
		dampingRateZeroThrottleClutchEngaged=2;
		dampingRateZeroThrottleClutchDisengaged=0.34999999;
		torqueCurve[]=
		{
			{0,0},
			{0.278,0.5},
			{0.34999999,0.75},
			{0.461,1},
			{0.60000002,0.94999999},
			{0.69999999,0.85000002},
			{0.80000001,0.75},
			{1,0.5}
		};
		changeGearMinEffectivity[]={0.94999999,0.15000001,0.94999999,0.94999999,0.94999999,0.94999999,0.94999999};
		switchTime=0.31;
		latency=1;
		class Wheels
		{
			class LF
			{
				boneName="wheel_1_1_damper";
				steering=1;
				side="left";
				center="wheel_1_1_axis";
				boundary="wheel_1_1_bound";
				width="0.126";
				mass=30;
				MOI=12.8;
				dampingRate=0.1;
				maxBrakeTorque=10000;
				maxHandBrakeTorque=0;
				suspTravelDirection[]={0,-1,0};
				suspForceAppPointOffset="wheel_1_1_axis";
				tireForceAppPointOffset="wheel_1_1_axis";
				maxCompression=0.050000001;
				mMaxDroop=0.1;
				sprungMass=825;
				springStrength=51625;
				springDamperRate=8920;
				longitudinalStiffnessPerUnitGravity=10000;
				latStiffX=25;
				latStiffY=180;
				frictionVsSlipGraph[]=
				{
					{0,1},
					{0.5,1},
					{1,1}
				};
			};
			class LR: LF
			{
				boneName="wheel_1_2_damper";
				steering=0;
				center="wheel_1_2_axis";
				boundary="wheel_1_2_bound";
				suspForceAppPointOffset="wheel_1_2_axis";
				tireForceAppPointOffset="wheel_1_2_axis";
				maxHandBrakeTorque=3500;
			};
			class RF: LF
			{
				boneName="wheel_2_1_damper";
				center="wheel_2_1_axis";
				boundary="wheel_2_1_bound";
				suspForceAppPointOffset="wheel_2_1_axis";
				tireForceAppPointOffset="wheel_2_1_axis";
				steering=1;
				side="right";
			};
			class RR: RF
			{
				boneName="wheel_2_2_damper";
				steering=0;
				center="wheel_2_2_axis";
				boundary="wheel_2_2_bound";
				suspForceAppPointOffset="wheel_2_2_axis";
				tireForceAppPointOffset="wheel_2_2_axis";
				maxHandBrakeTorque=3500;
			};
		};
		attenuationEffectType="CarAttenuation";
		soundGetIn[]={};
		soundGetOut[]={};
		soundDammage[]={};
		soundEngineOnInt[]={};
		soundEngineOnExt[]={};
		soundEngineOffInt[]={};
		soundEngineOffExt[]={};
		buildCrash0[]={};
		buildCrash1[]={};
		buildCrash2[]={};
		buildCrash3[]={};
		soundBuildingCrash[]={};
		WoodCrash0[]={};
		WoodCrash1[]={};
		WoodCrash2[]={};
		WoodCrash3[]={};
		WoodCrash4[]={};
		WoodCrash5[]={};
		soundWoodCrash[]={};
		ArmorCrash0[]={};
		ArmorCrash1[]={};
		ArmorCrash2[]={};
		ArmorCrash3[]={};
		soundArmorCrash[]={};
		class Sounds
		{
			class Idle_ext
			{
				sound[]={};
				frequency="0.95 + ((rpm/ 4500) factor[(800/ 4500),(1400/ 4500)])*0.15";
				volume="engineOn*camPos*(((rpm/ 4500) factor[(600/ 4500),(1100/ 4500)]) * ((rpm/ 4500) factor[(1800/ 4500),(1300/ 4500)]))";
			};
			class Engine
			{
				sound[]={};
				frequency="0.9 + ((rpm/ 4500) factor[(1400/ 4500),(2100/ 4500)])*0.2";
				volume="engineOn*camPos*(((rpm/ 4500) factor[(1400/ 4500),(1800/ 4500)]) * ((rpm/ 4500) factor[(2300/ 4500),(2000/ 4500)]))";
			};
			class Engine1_ext
			{
				sound[]={};
				frequency="0.9 +  ((rpm/ 4500) factor[(2100/ 4500),(2800/ 4500)])*0.2";
				volume="engineOn*camPos*(((rpm/ 4500) factor[(1900/ 4500),(2300/ 4500)]) * ((rpm/ 4500) factor[(3000/ 4500),(2500/ 4500)]))";
			};
			class Engine2_ext
			{
				sound[]={};
				frequency="0.9 + ((rpm/ 4500) factor[(2800/ 4500),(3600/ 4500)])*0.2";
				volume="engineOn*camPos*(((rpm/ 4500) factor[(2500/ 4500),(3100/ 4500)]) * ((rpm/ 4500) factor[(4500/ 4500),(3700/ 4500)]))";
			};
			class Engine3_ext
			{
				sound[]={};
				frequency="0.95 + ((rpm/ 4500) factor[(3600/ 4500),(4500/ 4500)])*0.1";
				volume="engineOn*camPos*((rpm/ 4500) factor[(3800/ 4500),(4500/ 4500)])";
			};
			class IdleThrust
			{
				sound[]={};
				frequency="0.95 + ((rpm/ 4500) factor[(800/ 4500),(1400/ 4500)])*0.15";
				volume="engineOn*camPos*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/ 4500) factor[(600/ 4500),(1100/ 4500)]) * ((rpm/ 4500) factor[(1800/ 4500),(1300/ 4500)]))";
			};
			class EngineThrust
			{
				sound[]={};
				frequency="0.9 + ((rpm/ 4500) factor[(1400/ 4500),(2100/ 4500)])*0.2";
				volume="engineOn*camPos*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/ 4500) factor[(1400/ 4500),(1800/ 4500)]) * ((rpm/ 4500) factor[(2300/ 4500),(2000/ 4500)]))";
			};
			class Engine1_Thrust_ext
			{
				sound[]={};
				frequency="0.9 +  ((rpm/ 4500) factor[(2100/ 4500),(2800/ 4500)])*0.2";
				volume="engineOn*camPos*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/ 4500) factor[(1900/ 4500),(2300/ 4500)]) * ((rpm/ 4500) factor[(3000/ 4500),(2500/ 4500)]))";
			};
			class Engine2_Thrust_ext
			{
				sound[]={};
				frequency="0.9 + ((rpm/ 4500) factor[(2800/ 4500),(3600/ 4500)])*0.2";
				volume="engineOn*camPos*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/ 4500) factor[(2500/ 4500),(3100/ 4500)]) * ((rpm/ 4500) factor[(4500/ 4500),(3700/ 4500)]))";
			};
			class Engine3_Thrust_ext
			{
				sound[]={};
				frequency="0.95 + ((rpm/ 4500) factor[(3600/ 4500),(4500/ 4500)])*0.1";
				volume="engineOn*camPos*(0.4+(0.6*(thrust factor[0.1,1])))*((rpm/ 4500) factor[(3800/ 4500),(4500/ 4500)])";
			};
			class Idle_int
			{
				sound[]={};
				frequency="0.95 + ((rpm/ 4500) factor[(800/ 4500),(1400/ 4500)])*0.15";
				volume="engineOn*(1-camPos)*(((rpm/ 4500) factor[(600/ 4500),(1100/ 4500)]) * ((rpm/ 4500) factor[(1800/ 4500),(1300/ 4500)]))";
			};
			class Engine_int
			{
				sound[]={};
				frequency="0.9 + ((rpm/ 4500) factor[(1400/ 4500),(2100/ 4500)])*0.2";
				volume="engineOn*(1-camPos)*(((rpm/ 4500) factor[(1400/ 4500),(1800/ 4500)]) * ((rpm/ 4500) factor[(2300/ 4500),(2000/ 4500)]))";
			};
			class Engine1_int
			{
				sound[]={};
				frequency="0.9 +  ((rpm/ 4500) factor[(2100/ 4500),(2800/ 4500)])*0.2";
				volume="engineOn*(1-camPos)*(((rpm/ 4500) factor[(1900/ 4500),(2300/ 4500)]) * ((rpm/ 4500) factor[(3000/ 4500),(2500/ 4500)]))";
			};
			class Engine2_int
			{
				sound[]={};
				frequency="0.9 + ((rpm/ 4500) factor[(2800/ 4500),(3600/ 4500)])*0.2";
				volume="engineOn*(1-camPos)*(((rpm/ 4500) factor[(2500/ 4500),(3100/ 4500)]) * ((rpm/ 4500) factor[(4500/ 4500),(3700/ 4500)]))";
			};
			class Engine3_int
			{
				sound[]={};
				frequency="0.95 + ((rpm/ 4500) factor[(3600/ 4500),(4500/ 4500)])*0.1";
				volume="engineOn*(1-camPos)*((rpm/ 4500) factor[(3800/ 4500),(4500/ 4500)])";
			};
			class IdleThrust_int
			{
				sound[]={};
				frequency="0.95 + ((rpm/ 4500) factor[(800/ 4500),(1400/ 4500)])*0.15";
				volume="engineOn*(1-camPos)*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/ 4500) factor[(600/ 4500),(1100/ 4500)]) * ((rpm/ 4500) factor[(1800/ 4500),(1300/ 4500)]))";
			};
			class EngineThrust_int
			{
				sound[]={};
				frequency="0.9 + ((rpm/ 4500) factor[(1400/ 4500),(2100/ 4500)])*0.2";
				volume="engineOn*(1-camPos)*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/ 4500) factor[(1400/ 4500),(1800/ 4500)]) * ((rpm/ 4500) factor[(2300/ 4500),(2000/ 4500)]))";
			};
			class Engine1_Thrust_int
			{
				sound[]={};
				frequency="0.9 +  ((rpm/ 4500) factor[(2100/ 4500),(2800/ 4500)])*0.2";
				volume="engineOn*(1-camPos)*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/ 4500) factor[(1900/ 4500),(2300/ 4500)]) * ((rpm/ 4500) factor[(3000/ 4500),(2500/ 4500)]))";
			};
			class Engine2_Thrust_int
			{
				sound[]={};
				frequency="0.9 + ((rpm/ 4500) factor[(2800/ 4500),(3600/ 4500)])*0.2";
				volume="engineOn*(1-camPos)*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/ 4500) factor[(2500/ 4500),(3100/ 4500)]) * ((rpm/ 4500) factor[(4500/ 4500),(3700/ 4500)]))";
			};
			class Engine3_Thrust_int
			{
				sound[]={};
				frequency="0.95 + ((rpm/ 4500) factor[(3600/ 4500),(4500/ 4500)])*0.1";
				volume="engineOn*(1-camPos)*(0.4+(0.6*(thrust factor[0.1,1])))*((rpm/ 4500) factor[(3800/ 4500),(4500/ 4500)])";
			};
			class TiresRockOut
			{
			};
			class TiresSandOut
			{
			};
			class TiresGrassOut
			{
			};
			class TiresMudOut
			{
			};
			class TiresGravelOut
			{
			};
			class TiresAsphaltOut
			{
			};
			class NoiseOut
			{
				sound[]={};
				frequency="1";
				volume="camPos*(damper0 max 0.02)*(speed factor[0, 8])";
			};
			class TiresRockIn
			{
			};
			class TiresSandIn
			{
			};
			class TiresGrassIn
			{
			};
			class TiresMudIn
			{
			};
			class TiresGravelIn
			{
			};
			class TiresAsphaltIn
			{
			};
			class NoiseIn
			{
				sound[]={};
				frequency="1";
				volume="(damper0 max 0.1)*(speed factor[0, 8])*(1-camPos)";
			};
			class breaking_ext_road
			{
			};
			class acceleration_ext_road
			{
			};
			class turn_left_ext_road
			{
			};
			class turn_right_ext_road
			{
			};
			class breaking_ext_dirt
			{
			};
			class acceleration_ext_dirt
			{
			};
			class turn_left_ext_dirt
			{
			};
			class turn_right_ext_dirt
			{
			};
			class breaking_int_road
			{
			};
			class acceleration_int_road
			{
			};
			class turn_left_int_road
			{
			};
			class turn_right_int_road
			{
			};
			class breaking_int_dirt
			{
			};
			class acceleration_int_dirt
			{
			};
			class turn_left_int_dirt
			{
			};
			class turn_right_int_dirt
			{
			};
		};
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
			{
			};
		};
		class HitPoints: HitPoints
		{
			class HitGlass1: HitGlass1
			{
				armor=0.1;
			};
			class HitGlass2: HitGlass2
			{
				armor=0.1;
			};
			class HitGlass3: HitGlass3
			{
				armor=0.1;
			};
			class HitGlass4: HitGlass4
			{
				armor=0.1;
			};
			class HitLFWheel: HitLFWheel
			{
				armor=0.38;
			};
			class HitLBWheel: HitLF2Wheel
			{
				armor=0.38;
			};
			class HitRFWheel: HitRFWheel
			{
				armor=0.38;
			};
			class HitRBWheel: HitRF2Wheel
			{
				armor=0.38;
			};
			class HitFuel
			{
				armor=1.4;
				material=-1;
				name="palivo";
				visual="";
				passThrough=1;
			};
		};
		class Damage
		{
			tex[]={};
			mat[]={};
		};
		class Reflectors
		{
			class Left
			{
				color[]={180,156,120};
				ambient[]={0.89999998,0.77999997,0.60000002};
				intensity=20;
				size=1;
				innerAngle=20;
				outerAngle=80;
				coneFadeCoef=5;
				position="flash";
				direction="flash dir";
				selection="flash_L";
				hitpoint="flash_L";
				useFlare=1;
				flareSize=1.4;
				flareMaxDistance="100.0f";
				dayLight=0;
				class Attenuation
				{
					start=0.5;
					constant=0;
					linear=0;
					quadratic=1.1;
					hardLimitStart=20;
					hardLimitEnd=30;
				};
			};
		};
		aggregateReflectors[]=
		{
			
			{
				"Left"
			}
		};
		class RenderTargets
		{
		};
	};
	class EOD9_HELMET_Lights_VEH: EOD9_HELMET_Lights_VEH_base
	{
		scope=2;
		faction="BLU_F";
		side=1;
		vehicleClass="Car";
		model="\EOD_SUIT\Models\EOD9_HELMET_Lights_VEH.p3d";
		displayname="EOD9_HELMET_Lights_VEH";
		hasGunner=0;
		transportSoldier=0;
		cargoAction[]={};
		threat[]={0,0,0};
		driverLeftHandAnimName="drivewheel";
		driverRightHandAnimName="";
		driverLeftLegAnimName="";
		driverRightLegAnimName="pedal_thrust";
		armor=120;
		damageResistance=0.030990001;
		Picture="";
		icon="";
		class Library
		{
			libTextDesc="EOD9_HELMET_Lights_VEH";
		};
		class Turrets: Turrets
		{
		};
		class AnimationSources: AnimationSources
		{
		};
		class UserActions
		{
		};
		class TransportWeapons
		{
		};
		class TransportItems
		{
		};
		class Damage
		{
			tex[]={};
			mat[]={};
		};
		HiddenSelections[]={};
		HiddenSelectionsTextures[]={};
	};
};
class cfgWeapons
{
	class InventoryItem_Base_F;
	class ItemCore;
	class UniformItem: InventoryItem_Base_F
	{
		type=801;
	};
	class VestItem: InventoryItem_Base_F
	{
		type=701;
		uniformType="Default";
		hiddenSelections[]={};
		armor=0;
		passThrough=1;
		hitpointName="HitBody";
	};
	class HeadgearItem: InventoryItem_Base_F
	{
		allowedSlots[]={901,605};
		type=605;
		hiddenSelections[]={};
		armor=0;
		passThrough=1;
		hitpointName="HitHead";
	};
	class EOD9_HELMET_Base: ItemCore
	{
		scope=2;
		weaponPoolAvailable=1;
		displayName="EOD9_HELMET_Base";
		picture="\EOD_SUIT\UI\EOD9_Helmet_Icon_ca.paa";
		model="\EOD_SUIT\Models\EOD9_HELMET.p3d";
		hiddenSelections[]=
		{
			"camo"
		};
		hiddenSelectionsTextures[]={};
		class ItemInfo: HeadgearItem
		{
			mass=40;
			uniformModel="\EOD_SUIT\Models\EOD9_HELMET.p3d";
			modelSides[]={6};
			armor=4;
			passThrough=0.5;
			hiddenSelections[]={};
		};
	};
	class EOD9_HELMET: EOD9_HELMET_Base
	{
		displayName="$STR_EOD9_HELMET";
		model="\EOD_SUIT\Models\EOD9_HELMET.p3d";
		hiddenSelections[]={};
		hiddenSelectionsTextures[]={};
		class ItemInfo: HeadgearItem
		{
			mass=800;
			uniformModel="\EOD_SUIT\Models\EOD9_HELMET.p3d";
			modelSides[]={6};
			armor=10000;
			passThrough=0;
			hiddenSelections[]={};
		};
	};
	class Uniform_EOD_Base: ItemCore
	{
		scope=0;
		allowedSlots[]={901};
		class ItemInfo: UniformItem
		{
			uniformModel="\EOD_SUIT\Models\EOD_Uniform.p3d";
			uniformClass="EOD_engineer";
			containerClass="Supply20";
			mass=0;
		};
	};
	class EOD_Uniform: Uniform_EOD_Base
	{
		scope=2;
		displayName="$STR_EOD_Uniform";
		picture="\EOD_SUIT\UI\EOD9_Uniform_Icon_ca.paa";
		model="\A3\Characters_F\Common\Suitpacks\suitpack_universal_F.p3d";
		hiddenSelections[]=
		{
			"camo"
		};
		hiddenSelectionsTextures[]=
		{
			"\EOD_SUIT\TexturesWIP\suitpack_EOD_Suit_co.paa"
		};
		class ItemInfo: UniformItem
		{
			uniformModel="-";
			uniformClass="EOD_engineer";
			containerClass="Supply20";
			mass=80;
		};
	};
	class EOD_SUIT_vest_base: ItemCore
	{
		scope=0;
		weaponPoolAvailable=1;
		allowedSlots[]={901};
		picture="\EOD_SUIT\UI\EOD9_Vest_Icon_ca.paa";
		model="\EOD_SUIT\Models\EOD_SUIT_vest.p3d";
		hiddenSelections[]={};
		class ItemInfo: VestItem
		{
			uniformmodel="\EOD_SUIT\Models\EOD_SUIT_vest.p3d";
			hiddenSelections[]={};
			containerClass="Supply0";
			mass=0;
			armor=0;
			passThrough=1;
		};
	};
	class EOD_SUIT_vest: EOD_SUIT_vest_base
	{
		scope=2;
		displayName="$STR_Vest_EOD";
		picture="\EOD_SUIT\UI\EOD9_Vest_Icon_ca.paa";
		model="\EOD_SUIT\Models\EOD_SUIT_vest.p3d";
		class ItemInfo: ItemInfo
		{
			uniformmodel="\EOD_SUIT\Models\EOD_SUIT_vest.p3d";
			containerClass="Supply40";
			mass=1000;
			armor=100000;
			passThrough=1e-006;
			class HitpointsProtectionInfo
			{
				class Neck
				{
					hitpointName="HitNeck";
					armor=8000;
					passThrough=0.00050000002;
				};
				class Arms
				{
					hitpointName="HitArms";
					armor=8000;
					passThrough=0.00050000002;
				};
				class Chest
				{
					hitpointName="HitChest";
					armor=8000;
					passThrough=9.9999997e-005;
				};
				class Diaphragm
				{
					hitpointName="HitDiaphragm";
					armor=8000;
					passThrough=9.9999997e-005;
				};
				class Abdomen
				{
					hitpointName="HitAbdomen";
					armor=80000;
					passThrough=9.9999997e-005;
				};
				class Body
				{
					hitpointName="HitBody";
					passThrough=9.9999997e-005;
				};
			};
		};
	};
};
class RscTitles
{
	class Default
	{
		idd=-1;
		fadein=0;
		fadeout=0;
		duration=0;
	};
	class RscPicture
	{
		access=0;
		idc=-1;
		type=0;
		style=48;
		colorBackground[]={0,0,0,0};
		colorText[]={1,1,1,1};
		font="puristaMedium";
		sizeEx=0;
		lineSpacing=0;
		fixedWidth=0;
		shadow=0;
		text="";
		x="safezoneX";
		y="safezoneY";
		w="safezoneW";
		h="safezoneH";
	};
	class RscEOD_Helmet_BaseTitle
	{
		idd=-1;
		text="EOD_SUIT\UI\EODS_EOD_SUIT_OVERLAY2_ca.paa";
		fadeIn=0.5;
		fadeOut=0.5;
		movingEnable="false";
		duration=9.9999998e+010;
		name="RscEOD_Helmet_BaseTitle";
		class controls;
	};
	class RscEOD_Helmet: RscEOD_Helmet_BaseTitle
	{
		idd=1044;
		name="RscEOD_Helmet";
		class controls
		{
			class EOD_HelmetImage: RscPicture
			{
				text="EOD_SUIT\UI\EODS_EOD_SUIT_OVERLAY2_ca.paa";
				idc=10650;
			};
		};
	};
	class RscEOD_Helmet_Cracked: RscEOD_Helmet_BaseTitle
	{
		idd=1045;
		name="RscEOD_Helmet_Cracked";
		class controls
		{
			class EOD_CrackedHelmetImage: RscPicture
			{
				text="EOD_SUIT\UI\EODS_EOD_SUIT_OVERLAY2_cracked_ca.paa";
				idc=10651;
			};
		};
	};
	class RscEOD_Interface: RscEOD_Helmet_BaseTitle
	{
		idd=1046;
		name="RscEOD_Helmet_Cracked";
		class controls
		{
			class EOD_CrackedHelmetImage: RscPicture
			{
				text="EOD_SUIT\UI\EOD_Suit_HandControl.paa";
				idc=10652;
			};
		};
	};
};
class RscBackPicture
{
	access=0;
	type=0;
	idc=-1;
	style=48;
	colorBackground[]={0,0,0,0};
	colorText[]={1,1,1,1};
	font="TahomaB";
	sizeEx=0;
	lineSpacing=0;
	text="";
	fixedWidth=0;
	shadow=0;
	x=0;
	y=0;
	w=0.2;
	h=0.15000001;
};
class HiddenButton
{
	access=0;
	type=1;
	text="";
	colorText[]={0,0,0,1};
	colorDisabled[]={0,0,0,0};
	colorBackground[]={0,0,0,0};
	colorBackgroundDisabled[]={0,0,0,0};
	colorBackgroundActive[]={0,0,0,0};
	colorFocused[]={0,0,0,0};
	colorShadow[]={0,0,0,0};
	colorBorder[]={0,0,0,0};
	soundEnter[]=
	{
		"\A3\ui_f\data\sound\RscButton\soundEnter",
		0.090000004,
		1
	};
	soundPush[]=
	{
		"\A3\ui_f\data\sound\RscButton\soundPush",
		0.5,
		1
	};
	soundClick[]=
	{
		"\A3\ui_f\data\sound\RscButton\soundClick",
		0.5,
		1
	};
	soundEscape[]=
	{
		"\A3\ui_f\data\sound\RscButton\soundEscape",
		0.090000004,
		1
	};
	style=2;
	x=0;
	y=0;
	w=0.095588997;
	h=0.039216001;
	shadow=2;
	font="PuristaMedium";
	sizeEx="(((((safezoneW / safezoneH) min 1.2) / 1.2) / 25) * 1)";
	offsetX=0.003;
	offsetY=0.003;
	offsetPressedX=0.0020000001;
	offsetPressedY=0.0020000001;
	borderSize=0;
};
class EOD_Suit_HandControl_Dialog
{
	idd=160791;
	enableSimulation="true";
	controlsBackground[]={};
	objects[]={};
	onLoad="if(sunOrMoon < 0.2)then{((_this select 0) displayCtrl 67676) ctrlSetText '\EOD_SUIT\UI\EOD_Suit_HandControl_n.paa';};";
	controls[]=
	{
		"background",
		"REDBREATHER_LED",
		"POWER_LED",
		"SOUND_LED",
		"LIGHT_LED",
		"ENERGY_LED",
		"SOUND_LED_R",
		"LIGHT_LED_R",
		"REDBREATHER_LED_R",
		"EOD_Suit_HandControl_B1",
		"EOD_Suit_HandControl_B2",
		"EOD_Suit_HandControl_B3",
		"EOD_Suit_HandControl_B4",
		"EOD_Suit_HandControl_B5",
		"EOD_Suit_HandControl_B_ON"
	};
	class background: RscBackPicture
	{
		idc=67676;
		text="EOD_SUIT\UI\EOD_Suit_HandControl.paa";
		x="-0.000281591 * safezoneW + safezoneX";
		y="-0.00125921 * safezoneH + safezoneY";
		w="1.00407 * safezoneW";
		h="1.00169 * safezoneH";
	};
	class REDBREATHER_LED: RscBackPicture
	{
		idc=67677;
		text="EOD_SUIT\UI\EOD_Suit_HandControl_RedbreatherLED.paa";
		onLoad="ctrlShow [67677, False]; [] spawn Script_EOD_SUIT_Ouverture_Interface;";
		x="-0.000281591 * safezoneW + safezoneX";
		y="-0.00125921 * safezoneH + safezoneY";
		w="1.00407 * safezoneW";
		h="1.00169 * safezoneH";
	};
	class POWER_LED: RscBackPicture
	{
		idc=67678;
		text="EOD_SUIT\UI\EOD_Suit_HandControl_PowerLED.paa";
		onLoad="ctrlShow [67678, False]; [] spawn Script_EOD_SUIT_Ouverture_Interface;";
		x="-0.000281591 * safezoneW + safezoneX";
		y="-0.00125921 * safezoneH + safezoneY";
		w="1.00407 * safezoneW";
		h="1.00169 * safezoneH";
	};
	class SOUND_LED: RscBackPicture
	{
		idc=67679;
		text="EOD_SUIT\UI\EOD_Suit_HandControl_SoundLED.paa";
		onLoad="ctrlShow [67679, False]; [] spawn Script_EOD_SUIT_Ouverture_Interface;";
		x="-0.000281591 * safezoneW + safezoneX";
		y="-0.00125921 * safezoneH + safezoneY";
		w="1.00407 * safezoneW";
		h="1.00169 * safezoneH";
	};
	class LIGHT_LED: RscBackPicture
	{
		idc=67680;
		text="EOD_SUIT\UI\EOD_Suit_HandControl_LightLED.paa";
		onLoad="ctrlShow [67680, False]; [] spawn Script_EOD_SUIT_Ouverture_Interface;";
		x="-0.000281591 * safezoneW + safezoneX";
		y="-0.00125921 * safezoneH + safezoneY";
		w="1.00407 * safezoneW";
		h="1.00169 * safezoneH";
	};
	class ENERGY_LED: RscBackPicture
	{
		idc=67681;
		text="EOD_SUIT\UI\EOD_Suit_HandControl_EnergyLED.paa";
		onLoad="ctrlShow [67681, False]; [] spawn Script_EOD_SUIT_Ouverture_Interface;";
		x="-0.000281591 * safezoneW + safezoneX";
		y="-0.00125921 * safezoneH + safezoneY";
		w="1.00407 * safezoneW";
		h="1.00169 * safezoneH";
	};
	class SOUND_LED_R: RscBackPicture
	{
		idc=67682;
		text="EOD_SUIT\UI\EOD_Suit_HandControl_SoundLED_R.paa";
		onLoad="ctrlShow [67682, False];";
		x="-0.000281591 * safezoneW + safezoneX";
		y="-0.00125921 * safezoneH + safezoneY";
		w="1.00407 * safezoneW";
		h="1.00169 * safezoneH";
	};
	class LIGHT_LED_R: RscBackPicture
	{
		idc=67683;
		text="EOD_SUIT\UI\EOD_Suit_HandControl_LightLED_R.paa";
		onLoad="ctrlShow [67683, False];";
		x="-0.000281591 * safezoneW + safezoneX";
		y="-0.00125921 * safezoneH + safezoneY";
		w="1.00407 * safezoneW";
		h="1.00169 * safezoneH";
	};
	class REDBREATHER_LED_R: RscBackPicture
	{
		idc=67684;
		text="EOD_SUIT\UI\EOD_Suit_HandControl_RedbreatherLED_R.paa";
		onLoad="ctrlShow [67684, False];";
		x="-0.000281591 * safezoneW + safezoneX";
		y="-0.00125921 * safezoneH + safezoneY";
		w="1.00407 * safezoneW";
		h="1.00169 * safezoneH";
	};
	class EOD_Suit_HandControl_B1: HiddenButton
	{
		idc="IDC_EOD_Suit_HandControl_B1";
		x="0.243016 * safezoneW + safezoneX";
		y="0.599815 * safezoneH + safezoneY";
		w="0.0273986 * safezoneW";
		h="0.0321852 * safezoneH";
		tooltip="$STR_Rebreather";
		action="[""Respirateur""] spawn Script_EOD_SUIT_Action_Bouton;";
	};
	class EOD_Suit_HandControl_B2: HiddenButton
	{
		idc="IDC_EOD_Suit_HandControl_B2";
		x="0.244775 * safezoneW + safezoneX";
		y="0.651223 * safezoneH + safezoneY";
		w="0.0251202 * safezoneW";
		h="0.0309259 * safezoneH";
		tooltip="$STR_Sound_System";
		action="[""Système sonore""] spawn Script_EOD_SUIT_Action_Bouton;";
	};
	class EOD_Suit_HandControl_B3: HiddenButton
	{
		idc="IDC_EOD_Suit_HandControl_B3";
		x="0.326254 * safezoneW + safezoneX";
		y="0.592519 * safezoneH + safezoneY";
		w="0.0282441 * safezoneW";
		h="0.0364815 * safezoneH";
		tooltip="$STR_Rebreather_External_System";
		action="[""Respirateur externe""] spawn Script_EOD_SUIT_Action_Bouton;";
	};
	class EOD_Suit_HandControl_B4: HiddenButton
	{
		idc="IDC_EOD_Suit_HandControl_B4";
		x="0.244254 * safezoneW + safezoneX";
		y="0.701593 * safezoneH + safezoneY";
		w="0.0256409 * safezoneW";
		h="0.0337038 * safezoneH";
		tooltip="$STR_Lights";
		action="[""Lumières""] spawn Script_EOD_SUIT_Action_Bouton;";
	};
	class EOD_Suit_HandControl_B5: HiddenButton
	{
		idc="IDC_EOD_Suit_HandControl_B5";
		x="0.326775 * safezoneW + safezoneX";
		y="0.644741 * safezoneH + safezoneY";
		w="0.0266822 * safezoneW";
		h="0.0346297 * safezoneH";
		tooltip="$STR_Heat_Regulation_System";
		action="[""Ventilation""] spawn Script_EOD_SUIT_Action_Bouton;";
	};
	class EOD_Suit_HandControl_B_ON: HiddenButton
	{
		idc="IDC_EOD_Suit_HandControl_B_ON";
		x="0.16022 * safezoneW + safezoneX";
		y="0.542149 * safezoneH + safezoneY";
		w="0.0277234 * safezoneW";
		h="0.0540742 * safezoneH";
		tooltip="$STR_Power";
		action="[""Alimentation""] spawn Script_EOD_SUIT_Action_Bouton;";
	};
};
