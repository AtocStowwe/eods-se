class CfgPatches
{
	class EODS_Boxes
	{
		units[]=
		{
			"EODS_Insurgent_BOX",
			"EODS_ARMY_BOX",
			"EODS_Ins_BOX_Base",
			"EODS_Us_BOX_Base",
			"EODS_Thor_BOX_Base",
			"EODS_THOR_BOX",
			"EODS_EOD_SUIT_BOX"
		};
		weapons[]={};
		requiredVersion=0.1;
		requiredAddons[]=
		{
			"CBA_XEH",
			"CBA_MAIN",
			"A3_UI_F",
			"A3_Weapons_F",
			"EODS_ieds_main"
		};
		magazines[]={};
		ammo[]={};
	};
};
class CfgDestructPos
{
	class DelayedDestructionAmmoBox
	{
		timeBeforeHiding="lifeTime/12";
		hideDuration="lifeTime/12";
	};
};
class CfgVehicles
{
	class ReammoBox;
	class ThingX;
	class Box_NATO_Support_F;
	class EODS_Ins_BOX_Base: Box_NATO_Support_F
	{
		mapSize=1.8099999;
		author="MrEwok";
		_generalMacro="EODS_Ins_BOX_Base";
		scope=1;
		displayName="EODS_Ins_BOX_Base";
		model="\EODS_BOXES\GuerillaCache.p3d";
		icon="iconCrate";
		maximumLoad=500;
		transportMaxWeapons=6;
		transportMaxMagazines=50;
		transportMaxBackpacks=12;
		transportAmmo=100;
		class TransportMagazines
		{
		};
		class TransportWeapons
		{
		};
		class TransportItems
		{
		};
	};
	class EODS_Us_BOX_Base: Box_NATO_Support_F
	{
		mapSize=1.8099999;
		author="MrEwok";
		_generalMacro="EODS_Us_BOX_Base";
		scope=1;
		displayName="Eods_GeneralPurpose_Box";
		model="\EODS_BOXES\Eods_GeneralPurpose_Box.p3d";
		icon="iconCrate";
		maximumLoad=500;
		transportMaxWeapons=6;
		transportMaxMagazines=50;
		transportMaxBackpacks=12;
		transportAmmo=100;
		class TransportMagazines
		{
		};
		class TransportWeapons
		{
		};
		class TransportItems
		{
		};
	};
	class EODS_Thor_BOX_Base: Box_NATO_Support_F
	{
		mapSize=1.8099999;
		author="MrEwok";
		_generalMacro="EODS_thor_BOX_Base";
		scope=1;
		displayName="EODS_thor_BOX_Base";
		model="\EODS_BOXES\eods_ThorBox.p3d";
		icon="iconCrate";
		maximumLoad=500;
		transportMaxWeapons=6;
		transportMaxMagazines=50;
		transportMaxBackpacks=12;
		transportAmmo=100;
		class TransportMagazines
		{
		};
		class TransportWeapons
		{
		};
		class TransportItems
		{
		};
	};
	class EODS_Insurgent_BOX: EODS_Ins_BOX_Base
	{
		author="MrEwok";
		scope=2;
		displayName="EODS Insurgent BOX";
		class TransportMagazines
		{
		};
		class TransportWeapons
		{
		};
		class TransportBackpacks
		{
		};
		class TransportItems
		{
			class EOD_BOX_cellphone_02
			{
				name="EODS_cellphone_02";
				count=10;
			};
			class EOD_BOX_ied_01
			{
				name="EODS_inventario_ied_01";
				count=5;
			};
			class EOD_BOX_ied_02
			{
				name="EODS_inventario_ied_02";
				count=5;
			};
			class EOD_BOX_ied_03
			{
				name="EODS_inventario_ied_03";
				count=5;
			};
			class EOD_BOX_ied_04
			{
				name="EODS_inventario_ied_04";
				count=5;
			};
			class EOD_BOX_ied_05
			{
				name="EODS_inventario_ied_05";
				count=5;
			};
			class EOD_BOX_ied_06
			{
				name="EODS_inventario_ied_06";
				count=5;
			};
			class EOD_BOX_ied_07
			{
				name="EODS_inventario_ied_07";
				count=5;
			};
			class EOD_BOX_ied_B_PressurePlate
			{
				name="EODS_inventario_ied_B_PressurePlate";
				count=5;
			};
			class EODS_Uxo_01_Inv_box
			{
				name="EODS_Uxo_01_Inv";
				count=5;
			};
		};
	};
	class EODS_ARMY_BOX: EODS_Us_BOX_Base
	{
		author="MrEwok";
		scope=2;
		displayName="EODS US ARMY BOX";
		class TransportMagazines
		{
		};
		class TransportWeapons
		{
		};
		class TransportBackpacks
		{
			class EOD_BOX_APOBS_A
			{
				count=1;
				backpack="APOBS_A";
			};
			class EOD_BOX_APOBS_B
			{
				count=1;
				backpack="APOBS_B";
			};
		};
		class TransportItems
		{
			class EOD_BOX_Talon_Remote
			{
				name="EODS_Talon_Remote";
				count=10;
			};
		};
	};
	class EODS_EOD_SUIT_BOX: EODS_Us_BOX_Base
	{
		author="MrEwok";
		scope=2;
		displayName="EODS EOD Suit BOX";
		class TransportMagazines
		{
		};
		class TransportWeapons
		{
		};
		class TransportBackpacks
		{
		};
		class TransportItems
		{
			class EOD9_HELMET_box
			{
				name="EOD9_HELMET";
				count=10;
			};
			class EOD_Uniform_box
			{
				name="EOD_Uniform";
				count=10;
			};
			class EOD_SUIT_vest_box
			{
				name="EOD_SUIT_vest";
				count=10;
			};
		};
	};
	class EODS_THOR_BOX: EODS_Thor_BOX_Base
	{
		author="MrEwok";
		scope=2;
		class AnimationSources
		{
			class Opn_Box
			{
				source="user";
				animPeriod=1;
				initPhase=0;
			};
		};
		class UserActions
		{
			class Opn_Box
			{
				displayNameDefault="$STR_EODS_BOX_ABRIR";
				displayName="$STR_EODS_BOX_ABRIR";
				position="Opn_Box_trig";
				radius=2;
				onlyForPlayer=0;
				condition="this animationPhase ""Opn_Box"" < 0.5";
				statement="this animate [""Opn_Box"", 1]";
			};
			class Close_box: Opn_Box
			{
				displayName="$STR_EODS_BOX_CERRAR";
				condition="this animationPhase ""Opn_Box"" >= 0.5";
				statement="this animate [""Opn_Box"", 0]";
			};
		};
		displayName="EODS US ARMY THOR BOX";
		class TransportMagazines
		{
		};
		class TransportWeapons
		{
		};
		class TransportBackpacks
		{
			class EOD_BOX_THOR_01
			{
				count=3;
				backpack="EODS_THORIII_OCP";
			};
			class EOD_BOX_THOR_02
			{
				count=4;
				backpack="EODS_THORIII_UCP";
			};
		};
		class TransportItems
		{
			class EODS_BOX_Inv_HighBandAntenna
			{
				name="EODS_Inv_HighBandAntenna";
				count=7;
			};
			class EODS_BOX_Inv_MidBandAntenna
			{
				name="EODS_Inv_MidBandAntenna";
				count=7;
			};
			class EODS_BOX_Inv_LowBandAntenna
			{
				name="EODS_Inv_LowBandAntenna";
				count=7;
			};
		};
	};
};
