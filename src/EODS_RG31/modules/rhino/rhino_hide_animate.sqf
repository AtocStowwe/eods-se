_car = _this select 0;

_car animate ["Rhino_hide", 0];

waitUntil {!alive _car};

_car animate ["Rhino_hide", 1];

waitUntil {!(isNull _car)};

[_car] execVM "Eods_RG31\modules\Rhino\Rhino_hide_animate.sqf";