_car = _this select 0;

_car animate ["Towing_Module", 0];

waitUntil {!alive _car};

_car animate ["Towing_Module", 1];

waitUntil {!(isNull _car)};

[_car] execVM "Eods_RG31\modules\Towing\Towing_Module_animate.sqf";